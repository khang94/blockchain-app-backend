
import * as csv from 'fast-csv';
import { IPortfolio, Portfolio } from '../model/Portfolio';
import { Transaction } from '../model/Transaction';
import { State } from '../state/State';


export const parseCSVFile = (filePath: string, state: State): State => {
  let transactions: Transaction[] = []

  csv.parseFile(filePath,
        { ignoreEmpty: true, trim: true, discardUnmappedColumns: true })
        .transform( function (data: any) {
          processData(data, transactions, state);
        })
        .on("data", function (data: any) {
        })
        .on("end", function () {
          console.log('end loading')
          console.log('Size of transaction history =>', transactions.length)
          console.log('State of portfolio ', JSON.stringify(state.portfolios))
          return state;
        })
  return state;
}

const processData = (data: any, transactionList: Transaction[], state: State): Transaction[] => {
  const isValidRow = validateRow(data);
  
  if (isValidRow) {
    const transaction = parseRowToTransaction(data);
    transactionList.push(transaction);

    const portfolio = parseRowToPortfolio(data);
    state.addPortfolio(portfolio)
  }
  
  return transactionList
}

const validateRow = (data: any) => {
  const [_, transactionType, token, amount] = data
  if (!transactionType || transactionType == '' || transactionType == 'transaction_type')
    return false;
  if (!token || token == '' || token == 'token')
    return false;
  if (!amount || amount == 0)
    return false;
  
  return true;
}

const parseRowToTransaction = (data: any): Transaction => {
  const [timestamp, transactionType, token, amount] = data
  return new Transaction(token, amount, transactionType, timestamp)
}

const parseRowToPortfolio = (data: any): IPortfolio => {
  const [_, txnType, token, amount] = data;
  return new Portfolio(token, 0, 0, 0, txnType, parseFloat(amount))
}