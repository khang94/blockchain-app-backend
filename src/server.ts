import * as express from 'express';
import * as http from 'http';
import * as WebSocket from 'ws';
import { AddressInfo } from 'net';
import { parseCSVFile } from './parse/Parser';
import Timer = NodeJS.Timer;
import { PriceCryptoService } from './service/PriceCryptoService';
import { IMarketMessage, MarketMessage } from './model/Market';
import { State } from './state/State';

const FILE_DATA_TRANSACTIONS = '/propine_test/blockchain-app-backend/src/file/transactions.csv'
const API_KEY = '44b5e3449166d11e97b28c1d08cc8517ae5f7f393825dbb7c0ee007ff55d8a7d'

const app = express();

// initialize a simple http server
const server = http.createServer(app);

// initialize the WebSocket server instance
const wss = new WebSocket.Server({ server });

let timer: Timer;

wss.on('connection', (ws: WebSocket) => {

    timer = setInterval(() => {
        if (ws && ws.readyState === WebSocket.OPEN) {
            let state = State.getInstance()
            ws.send(JSON.stringify(state))
        } else {
            clearInterval(timer);
        }
    }, 4); // ~ 256 Hz

});


const main = () => {

    let state = State.getInstance()

    const filePath = FILE_DATA_TRANSACTIONS
    state = parseCSVFile(filePath, state);

    const priceCryptoService = new PriceCryptoService([]);

    const ws = new WebSocket('wss://streamer.cryptocompare.com/v2?api_key=' + API_KEY);
        ws.addEventListener('open', function (event) {
        ws.send(JSON.stringify({
            "action": "SubAdd",
            "subs": ["5~CCCAGG~BTC~USD", "0~Coinbase~BTC~USD", "2~Binance~BTC~USDT",
                     "5~CCCAGG~ETH~USD", "0~Coinbase~ETH~USD", "2~Binance~ETH~USDT",
                     "5~CCCAGG~XRP~USD", "0~Coinbase~XRP~USD", "2~Binance~XRP~USDT"
                    ]
        }))
    });

    // Listen for messages
    ws.addEventListener('message', function (event) {
        const message = JSON.parse(event.data)
        let marketMessage: IMarketMessage;
        let tokenSymb: string = "";

        if ((message['TYPE'] == "5" || message['TYPE'] == "2") && message['FLAGS'] == "1") {

            tokenSymb = message["FROMSYMBOL"];
            marketMessage = new MarketMessage(
                message["TYPE"],
                message["MARKET"],
                message["FROMSYMBOL"],
                message["TOSYMBOL"],
                parseFloat(message["PRICE"]),
                parseInt(message["LASTUPDATE"]),
            );
            priceCryptoService.processMarketMessage(marketMessage);

        } else if (message['TYPE'] == 0) {
            
            tokenSymb = message["FSYM"];
            marketMessage = new MarketMessage(
                message["TYPE"],
                message["M"],
                message["FSYM"],
                message["TSYM"],
                parseFloat(message["P"]),
                parseInt(message["TS"]),
            )
            priceCryptoService.processMarketMessage(marketMessage);
        }


        // Update state
        if (message['FLAGS'] == "1" || message['TYPE'] == 0) {
            
            const currentPrice = priceCryptoService.calculateAveragePrice(tokenSymb)
        
            state.updatePricePortfolio(tokenSymb, currentPrice)
        }
                
    });

}

// start our server
server.listen(process.env.PORT || 8999, () => {
    main()
    console.log(`Server started on port ${(server.address() as AddressInfo).port} :)`);
});