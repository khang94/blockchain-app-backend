

import { IMarketMessage } from "../model/Market";

export class PriceCryptoService {
  
  private markets: IMarketMessage[] = [];

  constructor(markets: IMarketMessage[]) {
    this.markets = markets;
  }

  public setMarkets(markets: IMarketMessage[]) {
    this.markets = markets;
  }

  public processMarketMessage(marketMessage: IMarketMessage) {

    let marketExisted = false;
    for (let i = 0; i < this.markets.length; i++) {
      const currentMarketMsg = this.markets[i];
      if (currentMarketMsg.name == marketMessage.name
        && currentMarketMsg.fsym == marketMessage.fsym) {
        
        marketExisted = true;
        this.markets[i] = marketMessage;
      }
    }

    if (!marketExisted) {
      this.markets.push(marketMessage);
    }
  }

  public calculateAveragePrice(symb: string): number {
    
    let count = 0;
    let sumPrice = 0;
    for (let i = 0; i < this.markets.length; i++) {
      if (this.markets[i].fsym == symb) {
        count++;
        sumPrice += this.markets[i].price;
      }
    }

    console.log("Coin ", symb);
    console.log("Average Price ", sumPrice / count);
    const averagePrice = String(sumPrice / count);
    return parseFloat(parseFloat(averagePrice).toFixed(5));
  }


}