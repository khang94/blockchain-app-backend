interface ITransaction {
  timestamp: number;
  transactionType: string;
  tokenKey: string;
  amount: number
}

export class Transaction {
  
  timestamp: number;
  transactionType: string;
  tokenKey: string;
  amount: number;

  constructor(tokenKey: string, amount: number, transactionType: string, timestamp: number) {
    this.tokenKey = tokenKey;
    this.amount = amount;
    this.transactionType = transactionType;
    this.timestamp = timestamp;
  }
  
}

