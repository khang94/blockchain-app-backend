export interface IMarketMessage {
  id: string;
  name: string;
  fsym: string;
  tsym: string;
  price: number;
  timestamp: number;
}

export class MarketMessage {
  id: string;
  name: string;
  fsym: string;
  tsym: string;
  price: number;
  timestamp: number;

  constructor(id: string, name: string, fsym: string, tsym: string, price: number, timestamp: number) {
    this.id = id;
    this.name = name;
    this.fsym = fsym;
    this.tsym = tsym;
    this.price = price;
    this.timestamp = timestamp;
  }

}