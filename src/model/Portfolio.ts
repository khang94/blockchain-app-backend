export interface IPortfolio {
  token: string;
  value: number;
  balance: number;
  currentPrice: number;
  latestTransactionType: string;
  latestTransactionAmount: number;

  updatePrice(price: number): void;
}

export class Portfolio {
  token: string;
  value: number;
  balance: number;
  currentPrice: number;
  latestTransactionType: string;
  latestTransactionAmount: number;

  constructor(token: string, value: number,
    balance: number, currentPrice: number,
    latestTransactionType: string, latestTransactionAmount: number) {
    
    this.token = token;
    this.value = value;
    this.balance = balance;
    this.currentPrice = currentPrice;
    this.latestTransactionType = latestTransactionType;
    this.latestTransactionAmount = latestTransactionAmount;
  }

  public updatePrice(price: number): void {
    this.currentPrice = price;
    this.balance = this.currentPrice * this.value;
  }
}