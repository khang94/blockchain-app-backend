import { IPortfolio, Portfolio } from "../model/Portfolio";
import { Transaction } from "../model/Transaction";

export interface IState {
  transactions: Transaction[],
  portfolios: Portfolio[],
}

export class State {

  private static instance: State;

  public static getInstance(): State {
    if (!State.instance) {
      State.instance = new State([], [])
    }

    return State.instance
  }

  transactions: Transaction[];
  portfolios: Portfolio[];

  constructor(transactions: Transaction[], portfolios: Portfolio[]) {
    this.transactions = transactions;
    this.portfolios = portfolios;
  }

  public addTransaction(transaction: Transaction) {
    this.transactions.push(transaction);
  }

  public setTransactions(transactions: Transaction[]) {
    this.transactions = transactions
  }

  public addPortfolio(portfolio: IPortfolio) {
    
    let isPortfolioExisted = false;

    for (let i = 0; i < this.portfolios.length; i++) {
      if (this.portfolios[i].token == portfolio.token) {
        isPortfolioExisted = true;
        this.updatePortfolio(this.portfolios[i], portfolio, i)
        return;
      }
    }

    if (!isPortfolioExisted) {
      this.portfolios.push(portfolio)
    }
  }

  public updatePortfolio(currentPortfolio: Portfolio, portfolio: Portfolio, index: number): void {


    let updatedPortfolio = Object.assign({}, currentPortfolio)
    if (portfolio.latestTransactionType == "DEPOSIT") {
      updatedPortfolio.value = currentPortfolio.value + portfolio.latestTransactionAmount;
    } else {
      updatedPortfolio.value = currentPortfolio.value - portfolio.latestTransactionAmount;
    }
    
    updatedPortfolio.latestTransactionAmount = portfolio.latestTransactionAmount;
    updatedPortfolio.latestTransactionType = portfolio.latestTransactionType;
    this.portfolios[index] = updatedPortfolio;
  }

  public updatePricePortfolio(symb: string, currentPrice: number): void {

    for (let i = 0; i < this.portfolios.length; i++) {
      if (this.portfolios[i].token == symb) {
        
        const updatedPortfolio: Portfolio = Object.assign({}, this.portfolios[i]);
        updatedPortfolio.currentPrice = currentPrice;
        updatedPortfolio.balance = parseFloat(parseFloat(String(currentPrice * updatedPortfolio.value)).toFixed(5));

        this.portfolios[i] = updatedPortfolio;
      }
    }
  }
}